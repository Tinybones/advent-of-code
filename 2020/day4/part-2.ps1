<#
    --- Day 4: Passport Processing ---  
    https://adventofcode.com/2020/day/4
#>
#------------- Part 2 -------------#

$nl = [System.Environment]::NewLine
$PuzzleInput = (Get-content '.\input.txt' -Raw) -split ("$nl$nl") -replace " ", "`n"

# Part 2
function Test-Passport {
    [CmdletBinding()]
    param (
        [ValidateRange(1920, 2002)]
        $byr,
        [ValidateRange(2010, 2020)]
        $iyr,
        [ValidateRange(2020, 2030)]
        $eyr,
        [ValidatePattern("^\d+(cm|in)")]
        [ValidateScript( {
                $num = $_ -replace "\D"
                if ($_ -match "cm") {
                    if ($num -ge 150 -and $num -le 193) { $true }
                }
                else {
                    if ($num -ge 59 -and $num -le 76) { $true }
                }
            })]
        $hgt,
        [ValidatePattern("^#[\da-f]{6}$")]
        $hcl,
        [ValidateSet("amb", "blu", "brn", "gry", "grn", "hzl", "oth")]
        $ecl,
        [ValidatePattern("^\d{9}$")]
        $pid_,
        $cid
    )

    if ($byr -and $iyr -and $eyr -and $hgt -and $hcl -and $ecl -and $pid_) {
        return $true
    }
    else {
        return $false
    }
}

$ValidPassports = foreach ( $Passport in $PuzzleInput | ConvertFrom-StringData -Delimiter ':') {
    try { 
        Test-Passport @Passport -ErrorAction Stop
    }
    catch {}
}
$ValidPassports.Count
