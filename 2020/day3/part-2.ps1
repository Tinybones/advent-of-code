<#
    --- Day 3: Toboggan Trajectory ---    
    https://adventofcode.com/2020/day/3
#>

#------------- Part 3 -------------#
$map = Get-Content '.\input.txt'
$mapWidth = $map[0].Length -1 

$treecounttable = @()

$trialtable = @()
$trialtable += [PSCustomObject]@{Num = '1'; x = '1'; y = '1'}
$trialtable += [PSCustomObject]@{Num = '2'; x = '3'; y = '1'}
$trialtable += [PSCustomObject]@{Num = '3'; x = '5'; y = '1'}
$trialtable += [PSCustomObject]@{Num = '4'; x = '7'; y = '1'}
$trialtable += [PSCustomObject]@{Num = '5'; x = '1'; y = '2'}


foreach ($trial in $trialtable) {
    $position = 0

    # Create counter
    $treecount = 0

    foreach ($line in $map) {
        # Skip the first line
        if ($Map.IndexOf($Line) -eq 0) {
            $positionValue = $Line[0]
        }
        else {
            if ( ($Map.IndexOf($line)) % $trial.y -eq 0 ) {
                $position += $trial.x

                if ($position -gt $mapWidth) {
                    $position = $position - ($mapWidth + 1)
                }

                $positionValue = $Line[$position]
            }
            else {
                $positionValue = 'skip'
            }
        }
        
        switch ($positionValue) {
            '#' { $treecount++ }
            'skip' { } # Do nothing jusk skip
        }
    }
    # Add to the count
    $treecounttable += $treecount

    Write-Host "============== TriallNum = $($trial.Num) =============="
    Write-Host "Found: $($TreeCount) trees" -ForegroundColor Green
}


# Multiply final treecount
$final = 0
foreach ($tc in $treecounttable){
    if (!$final){
        $final = $tc
    }
    else {
        $final = $final * $tc
    }
}

Write-Host "Final answer: $($final)"

